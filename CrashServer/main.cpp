#include <QCoreApplication>

#include "singleshot.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <cassert>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTcpServer server;

    server.listen( QHostAddress::Any, 9999 );
    server.connect( &server, &QTcpServer::newConnection, [&]{
        while ( server.hasPendingConnections() )
        {
            QTcpSocket* socket = server.nextPendingConnection();
            qDebug() << "reply incoming" << socket->readAll();
            assert( socket );
            SingleShot::execute( 10 + (rand() % 200), [&, socket]{
                socket->write( "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\nfoobar" );
                SingleShot::execute(100, [&, socket]{
                    socket->close();
                    socket->deleteLater();
                });
            } );
        }
    } );

    return a.exec();
}
