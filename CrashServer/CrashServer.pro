#-------------------------------------------------
#
# Project created by QtCreator 2014-04-06T21:07:16
#
#-------------------------------------------------

QT       += core network

QT       -= gui

unix {
        QMAKE_CXXFLAGS += -U__STRICT_ANSI__
        QMAKE_CXXFLAGS += -std=c++0x
}

TARGET = CrashServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    singleshot.cpp

HEADERS += \
    singleshot.h
