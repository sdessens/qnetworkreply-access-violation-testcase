#ifndef SINGLESHOT_H
#define SINGLESHOT_H

#include <QObject>
#include <QTimer>

#include <functional>

/**
 * @brief Simple class that can be used to use QTimer::singleShot-like
 * functionality with lambda's as target
 */
class SingleShot : public QObject
{
    Q_OBJECT
    SingleShot(unsigned ms, std::function<void ()> fn);
public:
    static void execute( unsigned ms, std::function<void()> fn );

private:
    QTimer timer;
};

#endif // SINGLESHOT_H
