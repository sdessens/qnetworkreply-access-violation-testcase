#include "singleshot.h"

SingleShot::SingleShot( unsigned ms, std::function<void ()> fn)
{
    timer.setSingleShot( true );
    connect( &timer, &QTimer::timeout, [=]{ fn(); delete this; } );
    timer.start( ms );
}


void SingleShot::execute(unsigned ms, std::function<void ()> fn)
{
    new SingleShot( ms, fn );
}
