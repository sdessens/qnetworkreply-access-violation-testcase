#include "webclient.h"

#include <QNetworkRequest>
#include <QNetworkReply>

WebClient::WebClient()
    :   _requestsInFlight( 0 )
    ,   _totalRequests( 0 )
{
    _timer.start( 30 );
    connect( &_timer, &QTimer::timeout, this, &WebClient::tick );
}

void WebClient::tick()
{
    if ( _requestsInFlight < 3 )
    {
        request();
    }
}

void WebClient::request()
{
    _requestsInFlight++;

    QUrl url;
    url.setHost( "127.0.0.1" );
    url.setScheme( "http" );
    url.setPort( 9999 );
    QNetworkRequest request( url );
    QNetworkReply* reply = _manager.get( request );

    connect( reply, &QNetworkReply::finished, [=]{
        _requestsInFlight--;
        _totalRequests++;

        if ( ! (_totalRequests % 100) )
        {
            qDebug() << "made" << _totalRequests << "requests";
        }

        reply->deleteLater();
    } );
}
