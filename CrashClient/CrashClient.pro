#-------------------------------------------------
#
# Project created by QtCreator 2014-04-06T22:17:06
#
#-------------------------------------------------

QT       += core network

QT       -= gui

unix {
        QMAKE_CXXFLAGS += -U__STRICT_ANSI__
        QMAKE_CXXFLAGS += -std=c++0x
}

TARGET = CrashClient
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    webclient.cpp

HEADERS += \
    webclient.h
