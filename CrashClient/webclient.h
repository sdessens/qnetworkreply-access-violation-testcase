#ifndef WEBCLIENT_H
#define WEBCLIENT_H

#include <QObject>
#include <QTimer>
#include <QNetworkAccessManager>

class WebClient : public QObject
{
    Q_OBJECT
public:
    WebClient();

    void asyncRequest();

private slots:
    void tick();
    void request();

signals:
    void asyncRequestSignal();

private:
    int _requestsInFlight;
    unsigned long long _totalRequests;
    QTimer _timer;
    QNetworkAccessManager _manager;
};

#endif // WEBCLIENT_H
