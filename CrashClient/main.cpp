#include <QCoreApplication>

#include <QTimer>

#include "webclient.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    WebClient client;

    return a.exec();
}
